**Notifications**

Provides drop in back end for applications to easily add notifications features.

- Allows application users to subscribe to the interesting topics on various devices 
	
	console, sms, phone etc..

- Allows application to publish events when an interesting topic occurs
- Sends notifications to the subscribed users
- You can also setup device specific content templates


**Getting Started**

1. Prepare Database

	a. Create an emtpty database "Notifications"

	b. Run the Data/Notifications_Schema.sql file in the Notifications database

2. Run ActiveMQ.  
	
	Download activemq.  Open command line.  Go to its bin directory and type activemq.

3. Load the solution and run.  It starts two consoles Client and Publisher.
  
    Publisher app subscribes to a topic and sends a message to that topic
	Client app recieves the topic, finds the subscribers to that topic and delivers that message
	
4. Look at the tests for usage details.


Design:

Templates:

We are using NVelocity template language and parser.  All the templates must adhere to nvelocity syntax.
To understand the Templating look at VelocityTemplateParserTests.cs

Topics:

We support type safe topics.  This means programmer friendly subscription and publishing code.

Subscribe<YourTopic>();
Publish<YourTopic, YourTopicTemplateData>();

In the above example YourTopic should derive from a framework class TopicInfo.

public class  YourTopic : TopicInfo {}


