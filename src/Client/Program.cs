﻿namespace Client
{
    using System;

    using Notifications.Core;
    using Notifications.Service;

    class Program
    {
        static void Main(string[] args)
        {
            AutoFactory factory = new AutoFactory();
           using(var listener = factory.CreateListener(() => new TestUserResolver()))
            {
                listener.Start();
                Exit();
            }
        }

        static void Exit()
        {
            Console.WriteLine("Press any key to exit...");
            Console.ReadKey();
        }
    }
}
