﻿namespace Notifications.Devices
{
    using System;
    using System.Collections.Generic;

    using Notifications.Core;

    public class DeviceFactory : IDeviceFactory
    {
        private readonly DeviceTypeMapper deviceTypeMapper;

        public DeviceFactory(DeviceTypeMapper deviceTypeMapper)
        {
            this.deviceTypeMapper = deviceTypeMapper;
        }

        public IDevice GetDevice(DeviceType deviceType)
        {
            switch (deviceType)
            {
                case DeviceType.UnDefined:
                    return new ConsoleDevice();

                case DeviceType.Email:
                    return new EMailDevice();

                default:
                    throw new NotImplementedException();
            }
        }

        public IDevice GetDevice(Guid deviceId)
        {
            var deviceType = this.deviceTypeMapper.GetDeviceType(deviceId);
            return GetDevice(deviceType);
        }

        public Guid GetDeviceId(DeviceType deviceType)
        {
            if (deviceType == DeviceType.UnDefined)
            {
                deviceType = DeviceType.Email;
            }
            return this.deviceTypeMapper.GetDeviceGuid(deviceType);
        }

        public IEnumerable<KeyValuePair<DeviceType, Guid>> GetDevices()
        {
            return this.deviceTypeMapper.GetAllDeviceTypes();
        }
    }
}
