namespace Notifications.Devices
{
    using System;

    using Notifications.Core;

    public class ConsoleDevice : IDevice
    {
        public void Notify(string content)
        {
            Console.WriteLine(content);
        }
    }
}