namespace Notifications.Devices
{
    using System;

    using Notifications.Core;

    public class EMailDevice : IDevice
    {
        public void Notify(string content)
        {
            Console.WriteLine("From EmailNotifier:");
            Console.WriteLine(content);
        }
    }
}