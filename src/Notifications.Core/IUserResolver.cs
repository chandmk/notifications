﻿namespace Notifications.Core
{
    using System;
    using System.Collections.Generic;

    using Notifications.Core.Dto;

    /// <summary>
    /// Facade to get a list of users given their ids
    /// </summary>
    public interface IUserResolver
    {
        IEnumerable<UserInfo> GetUsers(IEnumerable<Guid> userIds);
    }
}
