﻿namespace Notifications.Core
{
    using System.Collections.Generic;

    /// <summary>
    /// Merges the message template variables with the given data.
    /// </summary>
    public interface ITemplateParser
    {
        string ApplyPlaceHolderValues(string template, string userName, IDictionary<string, object> templateValues);
    }
}
