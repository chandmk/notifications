﻿namespace Notifications.Core.Dto
{
    using System.Collections.Generic;
    using System;

    public class Topic
    {
        public Topic()
        {
            Id = Guid.NewGuid();
            IsActive = true;
        }
        public Guid Id { get; set; }

        public string TopicName { get; set; }

        public Guid? ParentTopicId { get; set; }

        public bool IsActive { get; set; }

        public virtual ICollection<Topic> ChildTopics { get; set; }
    }
}
