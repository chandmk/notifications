﻿namespace Notifications.Core.Dto
{
    using System;

    public class Device
    {
        public Device()
        {
            Id = Guid.NewGuid();
            IsActive = true;
        }
        public Guid Id { get; set; }

        public string Name { get; set; }

        public string Description { get; set; }

        public bool IsActive { get; set; }
    }
}
