﻿namespace Notifications.Core.Dto
{
    using System;

    public class Subscription
    {
        public Subscription()
        {
            Id = Guid.NewGuid();
        }
        public Guid Id { get; set; }

        public Guid UserId { get; set; }

        public Guid TopicId { get; set; }

        public Guid DeviceId { get; set; }

        public bool IsDigest { get; set; }

        public Topic Topic { get; set; }

        public Device Device { get; set; }
    }
}
