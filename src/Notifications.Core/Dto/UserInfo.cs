﻿namespace Notifications.Core.Dto
{
    using System;

    public class UserInfo
    {
        public Guid Id { get; set; }

        public string Name { get; set; }
        public string Description { get; set; }

        public string EMail { get; set; }
        public string InstantMessage { get; set; }
        public string Phone { get; set; }
        public string Fax { get; set; }
    }
}
