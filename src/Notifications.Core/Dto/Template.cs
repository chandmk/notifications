﻿namespace Notifications.Core.Dto
{
    using System;

    public class Template
    {
        public Template()
        {
            Id = Guid.NewGuid();
        }
        public Guid Id { get; set; }

        public string Name { get; set; }

        public string Description { get; set; }

        public Guid? DeviceId { get; set; }

        public string TemplateContent { get; set; }
    }
}
