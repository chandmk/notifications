﻿namespace Notifications.Core.Dto
{
    using System;
    using System.Collections.Generic;
    using System.Linq;

    public class TopicInfo
    {
        public Guid TopicId { get; set; }

        public string Path { get; set; }

        public ActionType Action { get; set; }

        public Guid Owner { get; set; }

        public virtual List<Tuple<ActionType, DeviceType, string>> Templates
        {
            get
            {
                return new List<Tuple<ActionType, DeviceType, string>>();
            }
        }

        public virtual string GetTemplate(DeviceType deviceType = DeviceType.Email)
        {
            string content = Templates.Where(t => t.Item1 == Action && t.Item2 == deviceType).Select(t => t.Item3).FirstOrDefault();
            return content ?? string.Empty;

        }

        public virtual string GenerateTemplatePath()
        {
            return string.Format("{0}.{1}", this.GetType().Name, Action);
        }

        public virtual string GenerateTopicName()
        {
            return string.Format("{0}:{1}", Path, Owner);
        }
    }
}
