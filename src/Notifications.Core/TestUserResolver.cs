namespace Notifications.Core
{
    using System;
    using System.Collections.Generic;
    using System.Linq;

    using Notifications.Core.Dto;

    public class TestUserResolver : IUserResolver
    {
        public IEnumerable<UserInfo> GetUsers(IEnumerable<Guid> userIds)
        {
            return userIds.Select(id => new UserInfo {Id = id, Name = "UserName " + id});
        }
    }
}