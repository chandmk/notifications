﻿namespace Notifications.Core
{
    public enum ActionType
    {
        All,
        Created,
        Updated,
        Deleted,
    }
}
