﻿namespace Notifications.Core
{
    using System.Collections.Generic;
    using System.Text;

    public class TestTemplateParser : ITemplateParser
    {
        public string ApplyPlaceHolderValues(string template, string userName, IDictionary<string, object> templateValues)
        {
            var templateSb = new StringBuilder(template);

            templateSb.AppendLine();
            templateSb.AppendFormat("for our dear {0}", userName);
            templateSb.AppendLine();

            foreach (var pair in templateValues)
            {
                templateSb.AppendFormat("{0}: {1}", pair.Key, pair.Value);
                templateSb.AppendLine();
            }

            return templateSb.ToString();
        }
    }
}
