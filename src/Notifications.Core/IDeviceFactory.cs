namespace Notifications.Core
{
    using System;
    using System.Collections.Generic;

    public interface IDeviceFactory
    {
        IDevice GetDevice(DeviceType deviceType);
        
        IDevice GetDevice(Guid deviceId);

        Guid GetDeviceId(DeviceType deviceType);

        IEnumerable<KeyValuePair<DeviceType, Guid>> GetDevices();
    }
}