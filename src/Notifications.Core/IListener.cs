﻿namespace Notifications.Core
{
    using System;

    /// <summary>
    /// Represents a consumer interested in receiving messages sent by publisher.  Clients must dispose this instance.
    /// </summary>
    public interface IListener : IDisposable
    {
        /// <summary>
        /// Used for checking the status of the Listener.
        /// </summary>
        /// <returns>Returns "OK" if publisher is alive.  Else returns exception message.</returns>
        string Ping();

        /// <summary>
        /// Starts this listner instance.
        /// </summary>
        void Start();

        /// <summary>
        /// Stops this listener instance. Calls Dispose().
        /// </summary>
        void Stop();
    }
}
