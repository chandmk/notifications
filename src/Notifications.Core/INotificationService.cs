﻿namespace Notifications.Core
{
    using System;

    using Notifications.Core.Dto;

    /// <summary>
    /// Facade that wraps subscription and publishing tasks.
    /// </summary>
    public interface INotificationService : IPublisher
    {
        /// <summary>
        /// Subscribes the specified topic.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="topic">The topic.</param>
        /// <param name="userId">The user id of subscriber interested in this topic</param>
        /// <param name="deviceType">Type of the device, user like to receive notifications</param>
        /// <returns></returns>
        bool Subscribe<T>(TopicInfo topic, Guid userId, DeviceType deviceType = DeviceType.Email) where T : TopicInfo;

        /// <summary>
        /// Unsubscribes the specified topic info.
        /// </summary>
        /// <param name="topicInfo">The topic info.</param>
        /// <param name="userId">The user id.</param>
        /// <returns></returns>
        bool Unsubscribe(TopicInfo topicInfo, Guid userId);

    }
}
