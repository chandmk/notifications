namespace Notifications.Core
{
    using System;

    /// <summary>
    /// Utility service to serialize a given object to string and deserialize the given string to object.
    /// </summary>
    public interface ISerializer
    {
        string Serialize<T>(T objectState);
        T DeSerialize<T>(string stringState);

        string Serialize(object objectState);
        object DeSerialize(Type type, string stringState);
    }
}