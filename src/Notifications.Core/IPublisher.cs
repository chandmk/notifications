﻿namespace Notifications.Core
{
    using Notifications.Core.Dto;

    /// <summary>
    /// Responsible for publishing a message
    /// </summary>
    public interface IPublisher
    {
        /// <summary>
        /// Used for checking the status of the publisher.
        /// </summary>
        /// <returns>Returns "OK" if publisher is alive.  Else returns exception message.</returns>
        string Ping();

        /// <summary>
        /// Publishes the specified topic info.
        /// </summary>
        /// <typeparam name="TTopic">The type of the topic.</typeparam>
        /// <typeparam name="TTemplateData">The type of the template data.</typeparam>
        /// <param name="topicInfo">The topic info. Base class for all topics in the system.</param>
        /// <param name="templateData">The template data.  This data is used to populate the placeholder variables of the message</param>
        void Publish<TTopic, TTemplateData>(TopicInfo topicInfo, TTemplateData templateData);
    }
}
