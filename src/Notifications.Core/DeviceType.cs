namespace Notifications.Core
{
    using System.ComponentModel;

    public enum DeviceType
    {
        [Description("Console Notification")]
        UnDefined = 0,

        [Description("Email Notification")]
        Email = 1,
    }
}