﻿namespace Notifications.Core
{
    /// <summary>
    /// Represents a device that is capable of recieving notifications
    /// </summary>
    public interface IDevice
    {
        void Notify(string content);
    }
}
