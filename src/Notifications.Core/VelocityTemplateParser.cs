﻿namespace Notifications.Core
{
    using System.Collections.Generic;

    using Vici.Core.Parser;
    using Vici.Core.Parser.Config;

    public class VelocityTemplateParser : ITemplateParser
    {
        public string ApplyPlaceHolderValues(string template, string userName, IDictionary<string, object> templateValues)
        {
            TemplateParser parser = new TemplateParser<Velocity>();
            IParserContext context = new CSharpContext(ParserContextBehavior.CaseInsensitiveVariables);
            context.Set("User", userName);
            foreach (var templateValue in templateValues)
            {
                context.Set(templateValue.Key, templateValue.Value);
            }
            string output = parser.Render(template, context);
            return output;
        }
    }
}
