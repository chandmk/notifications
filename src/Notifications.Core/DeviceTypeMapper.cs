namespace Notifications.Core
{
    using System;
    using System.Collections.Generic;
    using System.Linq;

    public class DeviceTypeMapper
    {
        private Dictionary<DeviceType, Guid> deviceMap = new Dictionary<DeviceType, Guid>();

        public DeviceTypeMapper()
        {
            this.deviceMap.Add(DeviceType.UnDefined, Guid.Empty);
            this.deviceMap.Add(DeviceType.Email, new Guid("6F3A19A7-144C-49DE-B9C8-E41CC6A82A12"));
        }

        public IEnumerable<KeyValuePair<DeviceType, Guid>> GetAllDeviceTypes()
        {
            return deviceMap.Where(m => m.Key != DeviceType.UnDefined);
        }

        public Guid GetDeviceGuid(DeviceType deviceType)
        {
            if (this.deviceMap.ContainsKey(deviceType))
            {
                return this.deviceMap[deviceType];
            }
            return Guid.Empty;
        }

        public DeviceType GetDeviceType(Guid deviceId)
        {
            var entry = this.deviceMap.Where(m => m.Value == deviceId).FirstOrDefault();
            return entry.Key;
        }
    }
}