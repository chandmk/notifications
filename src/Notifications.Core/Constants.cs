﻿namespace Notifications.Core
{
    public static class Constants
    {
        public const string TEMPLATE_DATA_TYPE_NAME = "_tempalte_data_type_name";

        public const string TOPIC_NAME = "_topic_name";

        public const string TOPIC_TEMPLATE_NAME = "_topic_template_name";
    }
}
