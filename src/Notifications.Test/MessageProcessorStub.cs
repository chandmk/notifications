﻿namespace Notifications.Test
{
    using System;

    using Apache.NMS;

    using NUnit.Framework;

    using Notifications.Core;
    using Notifications.Queue;

    public class MessageProcessorStub : IMessageProcessor
    {
        private readonly string expectedTopicName;

        private readonly string expectedTemplateName;

        private readonly string expectedTemplateDataTypeName;

        private readonly string expectedMessage;

        public MessageProcessorStub(string expectedTopicName, string expectedTemplateName, string expectedTemplateDataTypeName, string expectedMessage)
        {
            this.expectedTopicName = expectedTopicName;
            this.expectedTemplateName = expectedTemplateName;
            this.expectedTemplateDataTypeName = expectedTemplateDataTypeName;
            this.expectedMessage = expectedMessage;
        }

        public bool ReceiveMessage(ITextMessage message)
        {
            Assert.AreEqual(expectedTopicName, message.Properties.GetString(Constants.TOPIC_NAME));
            Assert.AreEqual(expectedTemplateName, message.Properties.GetString(Constants.TOPIC_TEMPLATE_NAME));
            Assert.AreEqual(expectedTemplateDataTypeName, message.Properties.GetString(Constants.TEMPLATE_DATA_TYPE_NAME));
            Assert.AreEqual(expectedMessage, message.Text);
            return true;
        }
    }
}