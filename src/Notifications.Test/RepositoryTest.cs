﻿namespace Notifications.Test
{
    using System;
    using System.Linq;

    using NUnit.Framework;

    using Notifications.Core;
    using Notifications.Core.Dto;
    using Notifications.DataAccess;

    [TestFixture]
    public class RepositoryTest : TransactionalTestBase
    {
        const string TestTopicCode = "T1";
        const string TestTemplate = "_test_template_._test_action_type_";

        INotificationRepository Repository = new NotificationRepository();

        [Test]
        [Ignore]
        public void TestRetrieveSubscriptions()
        {
            var subscriptions = Repository.RetrieveSubscriptions(TestTopicCode);

            Assert.That(subscriptions.Count() == 2);
            Assert.That(subscriptions.First().Topic.ChildTopics.Count == 2);
            Assert.That(subscriptions.Last().Topic.ChildTopics.Count == 2);
        }

        [Test]
        public void TestTemplateCrud()
        {
            var originalCount = Repository.Query<Template>("", "").Count();

            var template = NewTemplate();
            Guid templateId = Repository.Insert(template);
            Assert.That(templateId == template.Id);

            template = Repository.Retrieve<Template>(templateId);
            Assert.That(template.Name == TestTemplate);

            template.TemplateContent = "Test_Content_2";
            Repository.Update(template);
            template = Repository.Retrieve<Template>(templateId);
            Assert.AreEqual(template.TemplateContent, "Test_Content_2");

            var newCount = Repository.Query<Template>("", "").Count();
            Assert.That(newCount == originalCount + 1);

            Repository.Delete<Template>(templateId);
            newCount = Repository.Query<Template>("", "").Count();
            Assert.That(newCount == originalCount);
        }

        [Test]
        public void TestTopicCrud()
        {
            var originalCount = Repository.Query<Topic>("", "").Count();

            var topic = NewTopic();
            var topicId = Repository.Insert(topic);
            Assert.That(topicId != Guid.Empty);
            Assert.That(topicId == topic.Id);

            topic = Repository.Retrieve<Topic>(topicId);
            //Assert.That(topic.Name == TestTopic);
            Assert.IsTrue(topic.IsActive);

            topic.IsActive = false;
            Repository.Update(topic);
            topic = Repository.Retrieve<Topic>(topicId);
            Assert.IsFalse(topic.IsActive);

            var newCount = Repository.Query<Topic>("", "").Count();
            Assert.That(newCount == originalCount + 1);

            Repository.Delete<Topic>(topicId);
            newCount = Repository.Query<Topic>("", "").Count();
            Assert.That(newCount == originalCount);
        }

        [Test]
        public void TestSubscriptionCrud()
        {
            Guid subscriberId = Guid.NewGuid();

            var originalCount = Repository.Query<Subscription>("", "").Count();
            var deviceId = Repository.Query<Device>("Where [Name]='Email'", "").Single().Id;

            var topic = NewTopic();
            Repository.Insert(topic);

            var subscription = new Subscription { DeviceId = deviceId, TopicId = topic.Id, UserId = subscriberId, };
            Repository.Insert(subscription);

            var newCount = Repository.Query<Subscription>("", "").Count();
            Assert.That(newCount == originalCount + 1);

            Repository.Delete<Subscription>(subscription.Id);
            newCount = Repository.Query<Subscription>("", "").Count();
            Assert.That(newCount == originalCount);

            Repository.Delete<Topic>(topic.Id);
        }


        private Template NewTemplate()
        {
            return new Template { Name = TestTemplate, DeviceId = this.GetEmailDeviceId(), TemplateContent = "_test_content_", };
        }

      
        private static Topic NewTopic()
        {
            return new Topic { TopicName = "_test_topic_name_"};
        }
    }
}
