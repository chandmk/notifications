﻿namespace Notifications.Test
{
    using Notifications.Core;
    using Notifications.Core.Dto;

    public class PublisherStub : IPublisher
    {
        public string Ping()
        {
            return "OK";
        }

        public void Publish<TTopic, TTemplateData>(TopicInfo topicInfo, TTemplateData templateData)
        {
            throw new System.NotImplementedException();
        }
    }
}