﻿namespace Notifications.Test
{
    using System;
    using Notifications.Core;

    using NUnit.Framework;

    using Notifications.Core.Dto;

    [TestFixture]
    public class NotificationServiceTest : NotificationServiceTestBase
    {
        [Test]
        public void TestNotifcations()
        {
            var notificationService = this.CreateNotificationService();
            using (var listener = this.CreateListenerStub())
            {
                listener.Start();

                notificationService.Publish<TopicInfo, TopicState1>(GetTopic(TopicId1, ActionType.Created), new TopicState1 { Name = "P1", StringProperty1 = "S1", StringProperty12 = "S1-2", IntProperty1 = 1000, DateTimeProperty1 = new DateTime(2000, 1, 1) });
                notificationService.Publish<TopicInfo, TopicState2>(GetTopic(TopicId1, ActionType.Deleted), new TopicState2 { Name = "P2", StringProperty2 = "S2", StringProperty22 = "S2-2", IntProperty2 = 2000, DateTimeProperty2 = new DateTime(2000, 2, 1) });
                notificationService.Publish<TopicInfo, TopicState3>(GetTopic(TopicId2, ActionType.Updated), new TopicState3 { Name = "P3", StringProperty3 = "S3", StringProperty32 = "S3-2", IntProperty3 = 3000, DateTimeProperty3 = new DateTime(2000, 3, 1) });

                listener.Stop();
            }
        }

        [Test]
        public void TestSubscribe()
        {
            var service = this.CreateNotificationService();

            service.Subscribe<TopicInfo>(GetTopic(TopicId1, ActionType.Created), UserId1);
            service.Subscribe<TopicInfo>(GetTopic(TopicId2, ActionType.Deleted), UserId1);
            service.Subscribe<TopicInfo>(GetTopic(TopicId1, ActionType.Updated), UserId2);
        }

        [Test]
        public void TestUnSubscribe()
        {
            TestSubscribe();
            var service = this.CreateNotificationService();

            service.Unsubscribe(GetTopic(TopicId1, ActionType.Created), UserId1);
            service.Unsubscribe(GetTopic(TopicId1, ActionType.Deleted), UserId1);
            service.Unsubscribe(GetTopic(TopicId2, ActionType.Updated), UserId2);
        }

        private Guid TopicId1 = Guid.NewGuid();
        private Guid TopicId2 = Guid.NewGuid();
        private Guid UserId1 = Guid.NewGuid();
        private Guid UserId2 = Guid.NewGuid();
    }
}
