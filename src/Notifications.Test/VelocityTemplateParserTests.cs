﻿namespace Notifications.Test
{
    using System;
    using System.Collections.Generic;
    using System.Dynamic;

    using NUnit.Framework;
    using Notifications.Core;

    public class VelocityTemplateParserTests
    {
        [Test]
        public void ParsingTest()
        {
            var vtp = new VelocityTemplateParser();
            dynamic templateData = new ExpandoObject();
            templateData.AuthorName = "Doc Martin";
            templateData.BlogPostUrl = "BlogId1234";
            templateData.DateDeleted = DateTime.Now;
            templateData.Privacy = "Private";

            string output = vtp.ApplyPlaceHolderValues(
                "Dear ${User}, The user ${AuthorName} has deleted the blog post ${BlogPostUrl} on ${DateDeleted.ToShortDateString()}.  This post is currently ${Privacy}", 
                "test user", templateData);
            Assert.AreEqual("Dear test user, The user Doc Martin has deleted the blog post BlogId1234 on " + DateTime.Now.ToShortDateString() + ".  This post is currently Private", output);
        }
    }
}