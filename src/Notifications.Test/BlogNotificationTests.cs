namespace Notifications.Test
{
    using System;
    using NUnit.Framework;

    using Notifications.Core;
    using Notifications.Queue;
    using Notifications.Service;
    using Notifications.Topics;

    public class BlogNotificationTests : NotificationServiceTestBase
    {
        private Guid aBlog = Guid.NewGuid();
        private Guid aBlogPost = Guid.NewGuid();
        private Guid aBlogAuthor = Guid.NewGuid();
        private Guid aSubscriber = Guid.NewGuid();

        [Test]
        public void BasicMessageDeliveryTest()
        {
            var notificationService = this.CreateNotificationService();
            notificationService.Subscribe<BlogTopic>(new BlogTopic(aBlogAuthor, aBlog), aSubscriber);

            var blogPostTopic = new BlogPostTopic(Guid.NewGuid(), Guid.NewGuid(), Guid.NewGuid())
                { Action = ActionType.Created };
            var blogPostTemplateData = new BlogPostTemplateData("Doc Martin", "#", DateTime.Now);

            IMessageProcessor testProcessor = new MessageProcessorStub(
                                                blogPostTopic.GenerateTopicName(),
                                                blogPostTopic.GenerateTemplatePath(),
                                                blogPostTemplateData.GetType().AssemblyQualifiedName,
                                                new Serializer().Serialize(blogPostTemplateData));

            using (var nmsListener = this.CreateListenerStub(testProcessor))
            {
                nmsListener.Start();
                notificationService.Publish<BlogPostTopic, BlogPostTemplateData>(blogPostTopic, blogPostTemplateData);
            }
        }
    }
}