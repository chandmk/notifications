namespace Notifications.Test
{
    using System;

    using NUnit.Framework;

    using Notifications.Core;
    using Notifications.Core.Dto;
    using Notifications.Queue;
    using Notifications.Service;

    public class NotificationServiceTestBase : TransactionalTestBase
    {
        [TestFixtureSetUp]
        public void Setup()
        {
            var notifier = CreateNotificationService();
            string pingResult = notifier.Ping();
            if (string.Compare(pingResult, "OK", StringComparison.InvariantCultureIgnoreCase) != 0)
            {
                Assert.Ignore("ActiveMQ tests are being ignored. Reason: {0}", pingResult);
            }
        }
        protected TopicInfo GetTopic(Guid topicId, ActionType actionType)
        {
            return new TopicInfo { Path = "Root/Module/Presenter " + topicId, TopicId = topicId, Action = actionType, };
        }

        public INotificationService CreateNotificationService()
        {
            return new AutoFactory().CreateNotificationService();
        }

        public IListener CreateListenerStub(IMessageProcessor messageProcessor = null)
        {
            return new AutoFactory().CreateListener(() => new TestUserResolver(), messageProcessor);
        }

    }
}