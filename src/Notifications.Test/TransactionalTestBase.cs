namespace Notifications.Test
{
    using System;
    using System.Transactions;

    using NUnit.Framework;

    using Notifications.Core;
    using Notifications.Service;

    public class TransactionalTestBase
    {
        private TransactionScope transactionScope;

        [TestFixtureSetUp]
        public void RunFirst()
        {
            try
            {
                new AutoFactory().PopulateTemplatesAndDevices();
            }
            catch (Exception)
            {
             
            }
            
        }

        [SetUp]
        public void SetUp()
        {
            transactionScope = new TransactionScope(
                TransactionScopeOption.RequiresNew,
                new TransactionOptions() { IsolationLevel = IsolationLevel.ReadCommitted, Timeout = TimeSpan.FromSeconds(30) });
        }

        [TearDown]
        public void TearDown()
        {
            if (transactionScope != null)
            {
                transactionScope.Dispose();
            }
        }

        protected Guid GetEmailDeviceId()
        {
            var emailDeviceId = new DeviceTypeMapper().GetDeviceGuid(DeviceType.Email);
            return emailDeviceId;
        }
    }
}