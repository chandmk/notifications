﻿

namespace Notifications.Test
{
    using System;
    using System.Web;

    [Serializable]
    public class BlogPostTemplateData
    {
        public BlogPostTemplateData(string authorName, string blogUrl, DateTime dateAdded, string privacy = "Public")
        {
            AuthorName = authorName;
            BlogUrl = blogUrl;
            this.dateAdded = dateAdded;
            Privacy = privacy;
        }

        public string AuthorName {get; set;}

        public string BlogUrl { get; set; }

        public HtmlString BlogHtmlLink
        {
            get
            {
                return new HtmlString(string.Format("<a href='{0}'>blog</a>", BlogUrl));
            }
        }

        private DateTime dateAdded;

        public string DateAdded
        {
            get
            {
                return this.dateAdded.ToShortDateString();
            }
        }

        public string Privacy { get; set; }
    } 

    [Serializable]
    public class TopicState1
    {
        public TopicState1()
        {
            Name = "TopicState1";
        }

        public string Name { get; set; }
        public string StringProperty1 { get; set; }
        public int IntProperty1 { get; set; }
        public DateTime DateTimeProperty1 { get; set; }

        public string StringProperty12 { get; set; }
    }

    [Serializable]
    public class TopicState2
    {
        public TopicState2()
        {
            Name = "TopicState2";
        }

        public string Name { get; set; }

        public string StringProperty2 { get; set; }
        public int IntProperty2 { get; set; }
        public DateTime DateTimeProperty2 { get; set; }

        public string StringProperty22 { get; set; }
    }

    [Serializable]
    public class TopicState3
    {
        public TopicState3()
        {
            Name = "TopicState3";
        }

        public string Name { get; set; }

        public string StringProperty3 { get; set; }
        public int IntProperty3 { get; set; }
        public DateTime DateTimeProperty3 { get; set; }

        public string StringProperty32 { get; set; }
    }
}
