﻿
namespace Publisher
{
    using System;

    using Notifications.Core;
    using Notifications.Service;
    using Notifications.Topics;

    class Program
    {
        static void Main(string[] args)
        {
            Guid aBlog = Guid.NewGuid();
            Guid aBlogPost = Guid.NewGuid();
            Guid aBlogAuthor = Guid.NewGuid();
            Guid aSubscriber = Guid.NewGuid();
            AutoFactory factory = new AutoFactory();
            var notificationService = factory.CreateNotificationService();
            try
            {
                factory.PopulateTemplatesAndDevices();
            }
            catch (Exception)
            {
            }

            var blogPostTopic = new BlogPostTopic(aBlogAuthor, aBlog, aBlogPost);
            notificationService.Subscribe<BlogTopic>(blogPostTopic, aSubscriber);

            var blogPostTemplateData = new BlogPostTemplateData("Doc Martin", aBlogPost.ToString());

            blogPostTopic.Action = ActionType.Created;

            notificationService.Publish<BlogPostTopic, BlogPostTemplateData>(blogPostTopic, blogPostTemplateData);
            Console.WriteLine("Sending the message ... {0}", blogPostTopic.GenerateTopicName());
            Exit();
        }

        static void Exit()
        {
            Console.WriteLine("Press any key to exit...");
            Console.ReadKey();
        }
    }
}
