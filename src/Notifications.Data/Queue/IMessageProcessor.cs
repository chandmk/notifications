namespace Notifications.Queue
{
    using Apache.NMS;

    /// <summary>
    /// Responsible for processing the received message from ActiveMQ
    /// </summary>
    public interface IMessageProcessor
    {
        bool ReceiveMessage(ITextMessage message);
    }
}