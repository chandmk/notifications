namespace Notifications.Queue
{
    using System;

    using Apache.NMS;
    using Apache.NMS.ActiveMQ;
    using Apache.NMS.Policies;

    public class SimpleQueueListener : IDisposable
    {
        private IMessageConsumer consumer;
        private bool isDisposed;
        private readonly IMessageProcessor processor;
        private readonly ISession session;

        public SimpleQueueListener(IMessageConsumer consumer, IMessageProcessor processor, ISession session)
        {
            this.Consumer = consumer;
            this.processor = processor;
            this.session = session;
        }

        public IMessageConsumer Consumer
        {
            get
            {
                return this.consumer;
            }

            private set
            {
                this.consumer = value;
                MessageConsumer activeMqConsumer = value as MessageConsumer;
                if (activeMqConsumer != null)
                {
                    activeMqConsumer.RedeliveryPolicy = new RedeliveryPolicy { MaximumRedeliveries = 3 };
                }
                this.consumer.Listener += this.OnMessage;
            }
        }

        public void OnMessage(IMessage message)
        {
            ITextMessage textMessage = message as ITextMessage;
            try
            {
                if (this.processor.ReceiveMessage(textMessage))
                {
                    this.session.Commit();
                }
                else
                {
                    Console.WriteLine("Error - returning message to queue.");
                    this.session.Rollback();
                }
            }
            catch (Exception ex)
            {
                //treat the message as dead message
                Console.WriteLine(ex.Message);
                session.Commit();
            }
          
        }

        public void Dispose()
        {
            if (!this.isDisposed)
            {
                this.consumer.Dispose();
                this.isDisposed = true;
            }
        }

    }
}