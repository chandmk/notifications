﻿namespace Notifications.Queue
{
    using System;

    using Apache.NMS;
    using Apache.NMS.ActiveMQ.Commands;

    using Notifications.Core;

    public class SimpleQueuePublisher : IDisposable
    {
        private readonly IMessageProducer producer;
        private bool isDisposed = false;

        public SimpleQueuePublisher(IMessageProducer producer)
        {
            this.producer = producer;
        }

        public void SendMessage(string message, string messageType, string topicName, string topicTemplateName)
        {
            if (!this.isDisposed)
            {
                ITextMessage textMessage = new ActiveMQTextMessage(message);

                textMessage.Properties[Constants.TOPIC_NAME] = topicName;
                textMessage.Properties[Constants.TOPIC_TEMPLATE_NAME] = topicTemplateName;
                textMessage.Properties[Constants.TEMPLATE_DATA_TYPE_NAME] = messageType;

                this.producer.Send(textMessage);
            }
            else
            {
                throw new ObjectDisposedException(this.GetType().FullName);
            }
        }

        #region IDisposable Members

        public void Dispose()
        {
            if (!this.isDisposed)
            {
                this.producer.Dispose();
                this.isDisposed = true;
            }
        }

        #endregion
    }
}