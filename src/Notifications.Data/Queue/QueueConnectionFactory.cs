namespace Notifications.Queue
{
    using System.Configuration;

    using Apache.NMS;
    using Apache.NMS.ActiveMQ;

    public class QueueConnectionFactory
    {
        private readonly IConnectionFactory connectionFactory;

        public QueueConnectionFactory()
        {
            var brokerUri = ConfigurationManager.AppSettings["BrokerUri"];
            this.connectionFactory = new ConnectionFactory(brokerUri);
        }
        public QueueConnectionFactory(IConnectionFactory connectionFactory)
        {
            this.connectionFactory = connectionFactory;
        }

        public QueueConnection CreateConnection(string queueName = null)
        {
            if(string.IsNullOrEmpty(queueName))
            {
                queueName = ConfigurationManager.AppSettings["NotificationQueue"];
            }
            return new QueueConnection(this.connectionFactory, queueName);
        }

        public QueueConnection CreateTransactedConnection(string queueName = null)
        {
            if (string.IsNullOrEmpty(queueName))
            {
                queueName = ConfigurationManager.AppSettings["NotificationQueue"];
            }
            return new QueueConnection(this.connectionFactory, queueName, AcknowledgementMode.Transactional);
        }
    }
}