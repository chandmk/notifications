﻿namespace Notifications.Queue
{
    using System;

    using Apache.NMS;
    using Apache.NMS.ActiveMQ.Commands;

    /// <summary>
    /// Helper class to create publisher and consumer
    /// </summary>
    public class QueueConnection : IDisposable
    {
        private readonly IConnection connection;
        private readonly ISession session;
        private readonly IQueue queue;
        private bool isDisposed = false;

        public QueueConnection(IConnectionFactory connectionFactory, string queueName)
            : this(connectionFactory, queueName, AcknowledgementMode.AutoAcknowledge)
        {
        }

        public QueueConnection(IConnectionFactory connectionFactory, string queueName, AcknowledgementMode acknowledgementMode)
        {
            this.connection = connectionFactory.CreateConnection();
            this.connection.Start();
            this.session = this.connection.CreateSession(acknowledgementMode);
            this.queue = new ActiveMQQueue(queueName);
        }

        public SimpleQueuePublisher CreateSimpleQueuePublisher()
        {
            IMessageProducer producer = this.session.CreateProducer(this.queue);
            return new SimpleQueuePublisher(producer);
        }

        public SimpleQueueListener CreateSimpleQueueListener(IMessageProcessor processor)
        {
            IMessageConsumer consumer = this.session.CreateConsumer(this.queue);
            return new SimpleQueueListener(consumer, processor, this.session);
        }


        public void Dispose()
        {
            if (!this.isDisposed)
            {
                this.session.Dispose();
                this.connection.Stop();
                this.connection.Dispose();
                this.isDisposed = true;
            }
        }

    }
}