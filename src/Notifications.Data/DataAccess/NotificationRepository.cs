﻿
namespace Notifications.DataAccess
{
    using System;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.Linq;

    using Notifications.Core;
    using Notifications.Core.Dto;

    /// <summary>
    ///  Wraps all data access calls to Notification domain entities
    /// </summary>
    public class NotificationRepository : Repository, INotificationRepository
    {
        public IEnumerable<Subscription> RetrieveSubscriptions(string topicName)
        {
            var topic = this.RetrieveTopic(topicName, true, true);

            if (topic == null)
            {
                return new List<Subscription>();
            }

            var subscriptions = Query<Subscription>("Where TopicId=@0", "", topic.Id);

            if (subscriptions.First() == null)
            {
                return subscriptions;   // Empty list
            }

            foreach (var subscription in subscriptions)
            {
                subscription.Topic = topic;
            }

           // PopulateDevices(subscriptions);

            return subscriptions;
        }

        public Topic RetrieveTopic(TopicInfo topicInfo, bool activeOnly)
        {
            Debug.Assert(topicInfo != null);

            var topicCode = topicInfo.GenerateTopicName();

            return this.RetrieveTopic(topicCode, activeOnly);
        }

        public Topic CreateTopic(TopicInfo topicInfo)
        {
//            var template = Query<Template>("Id=@0 And ActionType=@1", "", topicInfo.TopicId, topicInfo.Action.ToString()).FirstOrDefault();
//
//            if (template == null)
//            {
//                throw new ArgumentException(string.Format("Template is not defined for topic id: {0} and action type: {1}", topicInfo.TopicId, topicInfo.Action));
//            }

            var topic = new Topic { TopicName = topicInfo.GenerateTopicName()};

            Insert(topic);

            return topic;
        }

        public bool SubscriptionExists(Subscription subscription)
        {
            var exist = Query<Subscription>("Where Id=@0 And Id=@1 And Id=@2", "",
                                            subscription.TopicId, subscription.UserId, subscription.DeviceId).FirstOrDefault();

            return exist != null;
        }

        public int DeleteSubscriptions(Guid topicId, Guid userId, DeviceType deviceType = DeviceType.UnDefined)
        {
            var table = CreateTable<Subscription>();

            if (deviceType == DeviceType.UnDefined)
            {
                return table.Delete(null, "Where Id=@0 And TopicId=@1", topicId, userId);
            }

            return table.Delete(null, "Where Id=@0 And TopicId=@1 And Id=@2", topicId, userId, (int)deviceType);
        }

        public string GetTemplateName(Topic topic)
        {
//            var templateName = topic.TemplateName;
//            if (string.IsNullOrEmpty(templateName))
//            {
//                if (topic.ParentTopicId != null)
//                {
//                    templateName = GetTemplateNameFromParentTopic(topic.ParentTopicId);
//                }
//            }
//            return templateName;
            return string.Empty;
        }

        public Template GetDeviceTemplate(string templateName, Guid? deviceId = null)
        {
            if(string.IsNullOrEmpty(templateName))
            {
                return null;
            }

            if(deviceId == null || deviceId == Guid.Empty)
            {
                return this.Query<Template>("Where Name=@0", templateName).FirstOrDefault();
            }

            return this.Query<Template>("Where Name=@0 And DeviceId=@1", "", templateName, deviceId.Value).FirstOrDefault();

        }

        private Topic RetrieveTopic(string topicName, bool activeOnly, bool withChildren=false)
        {
            var where = activeOnly ? "Where TopicName=@0 And IsActive=1" : "Where TopicName=@0";
            var topic = Query<Topic>(where, "", topicName).FirstOrDefault();

            if (topic != null && withChildren)
            {
                // Fetch all Topics -- consider cache
                var allTopics = Query<Topic>("IsActive=1", "");

                var childTopics = new List<Topic>();

                PopulateChildTopics(topic.Id, childTopics, allTopics);

                topic.ChildTopics = childTopics;
            }

            return topic;
        }        

        private void PopulateDevices(IEnumerable<Subscription> subscriptions)
        {
            var where = string.Format("Where Id In ({0}) And IsActive=1", GetIdList(subscriptions, s => s.DeviceId));
            var devices = Query<Device>(where, "").ToDictionary(s => s.Id);

            foreach (var subscription in subscriptions)
            {
                Device device;

                if (devices.TryGetValue(subscription.DeviceId, out device))
                {
                    subscription.Device = device;
                }                
            }
        }

        private static void PopulateChildTopics(Guid topicId, List<Topic> childTopicList, IEnumerable<Topic> allTopics)
        {
            Debug.Assert(topicId != Guid.Empty);
            Debug.Assert(childTopicList != null);

            var childTopics = allTopics.Where(p => p.ParentTopicId == topicId);

            childTopicList.AddRange(childTopics);

            foreach (var topic in childTopics)
            {
                // Recursion
                PopulateChildTopics(topic.Id, childTopicList, allTopics);
            }
        }

        // Todo: refactor to hierarchyid
        private string GetTemplateNameFromParentTopic(Guid? parentTopicId)
        {
//            if (parentTopicId == null || parentTopicId == Guid.Empty)
//            {
//                return string.Empty;
//            }
//
//            var parentTopic = this.Retrieve<Topic>((Guid)parentTopicId);
//            if (!string.IsNullOrEmpty(parentTopic.TemplateName))
//            {
//                return parentTopic.TemplateName;
//            }
//            return this.GetTemplateNameFromParentTopic(parentTopic.ParentTopicId);
            return string.Empty;
        }

        private static string GetIdList<T>(IEnumerable<T> entities, Func<T,Guid> idSelector)
        {
            return string.Join(",", entities.Select(idSelector).Distinct());
        }
    }
}
