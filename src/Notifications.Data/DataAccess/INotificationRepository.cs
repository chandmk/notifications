﻿namespace Notifications.DataAccess
{
    using System;
    using System.Collections.Generic;

    using Notifications.Core;
    using Notifications.Core.Dto;

    /// <summary>
    /// Wraps all data access calls to Notification domain entities
    /// </summary>
    public interface INotificationRepository : IRepository
    {
        IEnumerable<Subscription> RetrieveSubscriptions(string topicName);

        Topic RetrieveTopic(TopicInfo topicInfo, bool activeOnly);

        Topic CreateTopic(TopicInfo topicInfo);

        bool SubscriptionExists(Subscription subscription);

        int DeleteSubscriptions(Guid topicId, Guid userId, DeviceType deviceType = DeviceType.UnDefined);

        string GetTemplateName(Topic topic);

        Template GetDeviceTemplate(string templateName, Guid? deviceId = null);
    }
}
