﻿namespace Notifications.DataAccess
{
    using System;
    using System.Collections.Generic;
    using System.Dynamic;
    using System.Linq;

    public interface IRepository
    {
        IEnumerable<T> Query<T>(string where, string orderBy, params object[] args) where T : class, new();
        T Retrieve<T>(Guid id) where T : class, new();
        Guid Insert<T>(T entity) where T : class;
        void Update<T>(T entity) where T : class;
        void Delete<T>(Guid id) where T : class;
    }

    public class Repository : IRepository
    {
        public IEnumerable<T> Query<T>(string where, string orderBy, params object[] args) where T : class, new()
        {
            var table = CreateTable<T>();

            return table.All(where, orderBy, 0, "*", args)
                .Select(s => ((ExpandoObject)s).FromExpando<T>())
                .ToList();
        }

        public T Retrieve<T>(Guid id) where T : class, new()
        {
            var table = CreateTable<T>();

            var entity = table.Single(id);

            return ((ExpandoObject)entity).FromExpando<T>();
        }

        public Guid Insert<T>(T entity) where T : class
        {
            var table = CreateTable<T>();
            table.Insert(entity);
            return (Guid) GetEntityId(entity, table.PrimaryKeyField);
        }

        public void Update<T>(T entity) where T : class
        {
            var table = CreateTable<T>();
            var id = GetEntityId(entity, table.PrimaryKeyField);

            table.Update(entity, id);
        }

        public void Delete<T>(Guid id) where T : class
        {
            var table = CreateTable<T>();

            table.Delete(id);
        }

        public void Delete<T>(string where, params object[] args) where T : class
        {
            var table = CreateTable<T>();

            table.Delete(null, where, args);
        }


        protected static DynamicModel CreateTable<T>() where T : class
        {
            return DynamicModelFactory.CreateTable<T>();
        }

        private static void SetEntityId(object entity, string keyField, Guid id)
        {
            var idProperty = entity.GetType().GetProperty(keyField);

            idProperty.SetValue(entity, id, null);
        }

        private static object GetEntityId(object entity, string keyField)
        {
            var idProperty = entity.GetType().GetProperty(keyField);

            return idProperty.GetValue(entity, null);
        }
    }
}
