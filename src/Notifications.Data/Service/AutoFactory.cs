﻿using Notifications.DataAccess;
using Notifications.Queue;

namespace Notifications.Service
{
    using System;
    using System.Linq;
    using System.Reflection;

    using Notifications.Core;
    using Notifications.Core.Dto;
    using Notifications.Devices;

    public class AutoFactory
    {
        public INotificationService CreateNotificationService()
        {
            INotificationRepository repository = new NotificationRepository();
            ISerializer serializer = new Serializer();
            IDeviceFactory deviceFactory = new DeviceFactory(new DeviceTypeMapper());
            QueueConnectionFactory connectionFactory = new QueueConnectionFactory();
            Func<IPublisher> publisherFactory = () => new NmsPublisher(connectionFactory, serializer);

            return new NotificationService(repository, deviceFactory, publisherFactory);
        }

        public IListener CreateListener(Func<IUserResolver> userResolverFactory, IMessageProcessor messageProcessor= null)
        {
            QueueConnectionFactory connectionFactory = new QueueConnectionFactory();

            if (messageProcessor == null)
            {
                INotificationRepository repository = new NotificationRepository();
                ISerializer serializer = new Serializer();
                IDeviceFactory deviceFactory = new DeviceFactory(new DeviceTypeMapper());
                ITemplateParser templateParser = new VelocityTemplateParser();
                messageProcessor = new NmsMessageProcessor(
                    repository, serializer, deviceFactory, templateParser, userResolverFactory);
            }

            return new NmsListener(connectionFactory, messageProcessor );
        }

        public void PopulateTemplatesAndDevices()
        {
            var deviceId = InsertDefaultDevices();

//            InsertDefaultTemplates(deviceId);
        }

        private static Guid InsertDefaultDevices()
        {
            INotificationRepository repository = new NotificationRepository();

            var deviceId = new DeviceTypeMapper().GetDeviceGuid(DeviceType.Email);
            var device = new Device() { Id = deviceId, Name = "Email" };
            repository.Insert(device);
            return deviceId;
        }

        private static void InsertDefaultTemplates(Guid deviceId)
        {
            INotificationRepository repository = new NotificationRepository();

            var topics =
                Assembly.Load("Notifications.Topics").GetExportedTypes().Where(t => typeof(TopicInfo).IsAssignableFrom(t));
            foreach (var topicType in topics)
            {
                foreach (var actionType in Enum.GetNames(typeof(ActionType)))
                {
                    if (actionType == ActionType.All.ToString())
                    {
                        continue;
                    }
                    var template = new Template()
                        {
                            DeviceId = deviceId,
                            Name = string.Format("{0}.{1}", topicType.Name, actionType),
                            TemplateContent = string.Format("Dear <user>, Message: {0} {1}", topicType.Name, actionType)
                        };

                    repository.Insert(template);
                }
            }
        }
    }
}
