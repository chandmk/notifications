﻿namespace Notifications.Service
{
    using System;

    using Notifications.Core;
    using Notifications.Core.Dto;
    using Notifications.Queue;

    public class NmsPublisher : IPublisher
    {
        private readonly QueueConnectionFactory _connectionFactory;
        private readonly ISerializer _serializer;

        public NmsPublisher(QueueConnectionFactory connectionFactory, ISerializer serializer)
        {
            _connectionFactory = connectionFactory;
            _serializer = serializer;
        }

        public string Ping()
        {
            QueueConnection connection = null;
            try
            {
                connection = _connectionFactory.CreateConnection();
                return "OK";
            }
            catch (Exception e)
            {
                return e.Message;
            }
            finally
            {
                if (connection != null)
                {
                    connection.Dispose();
                }
            }
        }

        private void Publish<TTemplateData>(string topicName, TTemplateData templateData, string topicTemplateName)
        {
            var templateDataTypeName = typeof(TTemplateData).AssemblyQualifiedName;
            var serializedTemplateData = _serializer.Serialize(templateData);

            using (var connection = _connectionFactory.CreateConnection())
            {
                using (var publisher = connection.CreateSimpleQueuePublisher())
                {
                    publisher.SendMessage(serializedTemplateData, templateDataTypeName, topicName, topicTemplateName);
                }
            }
        }

        public void Publish<TTopic, TTemplateData>(TopicInfo topicInfo, TTemplateData templateData)
        {
            Publish(topicInfo.GenerateTopicName(), templateData, topicInfo.GenerateTemplatePath());
        }

       
    }
}
