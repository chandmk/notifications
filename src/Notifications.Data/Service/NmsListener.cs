﻿
namespace Notifications.Service
{
    using System;

    using Notifications.Core;
    using Notifications.Queue;

    public class NmsListener : IListener
    {
        private readonly QueueConnectionFactory _connectionFactory;
        private readonly IMessageProcessor _messageProcessor;

        private QueueConnection _connection;
        private SimpleQueueListener _listener;

        public NmsListener(QueueConnectionFactory connectionFactory, IMessageProcessor messageProcessor)
        {
            _connectionFactory = connectionFactory;
            _messageProcessor = messageProcessor;
        }

        public string Ping()
        {
            QueueConnection connection = null;
            try
            {
                connection = _connectionFactory.CreateConnection();
                return "OK";
            }
            catch (Exception e)
            {
                return e.Message;
            }
            finally
            {
                if (connection != null)
                {
                    connection.Dispose();
                }
            }
        }

        public void Start()
        {
            _connection = _connectionFactory.CreateTransactedConnection();
            _listener = _connection.CreateSimpleQueueListener(_messageProcessor);
        }

        public void Stop()
        {
            _listener.Dispose();
            _connection.Dispose();
        }

        public void Dispose()
        {
            Stop();
        }
    }
}
