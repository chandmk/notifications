﻿
namespace Notifications.Service
{
    using System;
    using System.Linq;

    using Notifications.Core;
    using Notifications.Core.Dto;
    using Notifications.DataAccess;

    /// <summary>
    /// Facade that wraps subscription and publishing tasks.
    /// </summary>
    public class NotificationService : INotificationService
    {
        private readonly INotificationRepository repository;
        private readonly IDeviceFactory deviceFactory;
        private readonly Func<IPublisher> publisherFactory;

        public NotificationService(INotificationRepository repository,
                                IDeviceFactory deviceFactory,
                                Func<IPublisher> publisherFactory)
        {
            this.repository = repository;
            this.deviceFactory = deviceFactory;
            this.publisherFactory = publisherFactory;
        }

        public string Ping()
        {
            return this.Publisher.Ping();
        }

        public bool Subscribe<T>(TopicInfo topicInfo, Guid userId, DeviceType deviceType = DeviceType.Email) where T : TopicInfo
        {
            var topic = this.repository.RetrieveTopic(topicInfo, true);

            if (topic == null)
            {
                topic = this.repository.CreateTopic(topicInfo);
            }

            var subscription = new Subscription { TopicId = topic.Id, UserId = userId, DeviceId = deviceFactory.GetDeviceId(deviceType), };

            if (this.repository.SubscriptionExists(subscription))
            {
                return false;
            }

            this.repository.Insert(subscription);

            return true;
        }

        public bool Unsubscribe(TopicInfo topicInfo, Guid userId)
        {
            var topic = this.repository.RetrieveTopic(topicInfo, false);

            if (topic == null)
            {
                return false;
                //throw new Exception(string.Format("Topic with code: [{0}] was not defined", topic.GenerateTopicName()));
            }

            return this.repository.DeleteSubscriptions(topic.Id, userId) > 0;
        }

        public void Publish<TTopic, TTemplateData>(TopicInfo topicInfo, TTemplateData templateData)
        {
            InsertTemplateContent(topicInfo);

            this.Publisher.Publish<TTopic, TTemplateData>(topicInfo, templateData);
        }

        private void InsertTemplateContent(TopicInfo topicInfo)
        {
            //Todo Cache the template names

            string templateName = topicInfo.GenerateTemplatePath();
            if (string.IsNullOrEmpty(templateName)) return;
            foreach (var device in deviceFactory.GetDevices())
            {
                var deviceId = device.Value;
                var foundTemplate = this.repository.Query<Template>("Where name=@0 and deviceid=@1", string.Empty, templateName, device.Value).Count() > 0;
                if (foundTemplate)
                {
                    continue;
                }
                string templateContent = topicInfo.GetTemplate(device.Key);
                var template = new Template { DeviceId = deviceId, Name = templateName, TemplateContent = templateContent };
                this.repository.Insert(template);
            }
        }

        private IPublisher Publisher
        {
            get
            {
                return publisherFactory();
            }
        }

    }
}
