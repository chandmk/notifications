﻿

namespace Notifications.Service
{
    using System;

    using Apache.NMS;

    using Notifications.Core.Dto;
    using Notifications.Queue;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.Linq;

    using Notifications.Core;
    using Notifications.DataAccess;

    public class NmsMessageProcessor : IMessageProcessor
    {
        private readonly ISerializer serializer;

        private readonly ITemplateParser templateParser;

        private readonly INotificationRepository repository;

        private readonly IDeviceFactory deviceFactory;

        private readonly Func<IUserResolver> userResolverFactory;

        public NmsMessageProcessor(INotificationRepository repository,
            ISerializer serializer,
            IDeviceFactory deviceFactory,
            ITemplateParser templateParser,
            Func<IUserResolver> userResolverFactory)
        {
            this.repository = repository;
            this.serializer = serializer;
            this.deviceFactory = deviceFactory;
            this.templateParser = templateParser;
            this.userResolverFactory = userResolverFactory;
        }


        public bool ReceiveMessage(ITextMessage message)
        {
            var topicName = message.Properties.GetString(Constants.TOPIC_NAME);
            if (string.IsNullOrEmpty(topicName))
                return true;

            //var templateName = this.repository.GetTemplateName(topic);

            var topicTemplateName = message.Properties.GetString(Constants.TOPIC_TEMPLATE_NAME);

            var templateDataType = message.Properties.GetString(Constants.TEMPLATE_DATA_TYPE_NAME);


            var msgText = message.Text;
            Console.WriteLine("Received Message: {0}\n", topicName);
            Debug.Assert(!string.IsNullOrEmpty(templateDataType));
            var templateData = this.GetTemplateDataInstanceFromMessage(templateDataType, msgText);
            var templateValues = templateData.ToExpando();
            if (templateData == null)
            {
                Console.WriteLine("\n\t {0} Template has no content", templateDataType);
                return true;
            }

            var subscriptions = this.repository.RetrieveSubscriptions(topicName);

            if (subscriptions.Count() == 0)
            {
                Console.WriteLine("\n\t No Subscribers for this message");
                return true;
            }

            var userInfoDictionary = userResolverFactory().GetUsers(subscriptions.Select(s => s.UserId).Distinct()).ToDictionary(u => u.Id);
            var deviceGroups = subscriptions.GroupBy(s => s.DeviceId);

            foreach (var deviceGroup in deviceGroups)
            {
                var deviceId = deviceGroup.Key;

                var template = this.repository.GetDeviceTemplate(topicTemplateName, deviceId);
                var templateContent = template != null ? template.TemplateContent : string.Empty;
                var device = this.deviceFactory.GetDevice(deviceId);

                foreach (var subscription in deviceGroup)
                {
                    var userInfo = userInfoDictionary[subscription.UserId];
                    var content = this.templateParser.ApplyPlaceHolderValues(templateContent, userInfo.Name, templateValues);

                    device.Notify(content);
                }
            }
            return true;
        }

        private Template GetTemplate(List<Template> templateList, Guid templateId)
        {
            Debug.Assert(templateList != null);

            var template = templateList.Find(t => t.Id == templateId);

            if (template == null)
            {
                template = this.repository.Retrieve<Template>(templateId);

                templateList.Add(template);
            }

            return template;
        }

        private object GetTemplateDataInstanceFromMessage(string tempalteDataTypeName, string messagee)
        {
            if (string.IsNullOrWhiteSpace(tempalteDataTypeName))
            {
                return null;
            }

            var templateDataType = Type.GetType(tempalteDataTypeName);
            if (templateDataType == null)
            {
                return null;
            }
            return this.serializer.DeSerialize(templateDataType, messagee);
        }

        public bool ErrorOnNextMessage { get; set; }

        public int NumberOfErrors { get; set; }
    }
}
