﻿using System;
using System.IO;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Json;
using System.Text;

namespace Notifications.Service
{
    using Notifications.Core;

    public interface ISerializerProvider
    {
        XmlObjectSerializer Provide(Type type);
    }

    public class Serializer : ISerializer
    {
        private readonly ISerializerProvider _provider;

        public Serializer(ISerializerProvider provider = null)
        {
            _provider = provider ?? new JsonSerializerProvider();
        }

        public string Serialize<T>(T objectState)
        {
            return Serialize(_provider, objectState);
        }

        public T DeSerialize<T>(string stringState)
        {
            return (T)DeSerialize(_provider, typeof(T), stringState);
        }

        public string Serialize(object objectState)
        {
            return Serialize(_provider, objectState);
        }

        public object DeSerialize(Type type, string stringState)
        {
            return DeSerialize(_provider, type, stringState);
        }


        private static string Serialize(ISerializerProvider provider, object objectState)
        {
            if (objectState == null)
            {
                return "";
            }
            var serializer = provider.Provide(objectState.GetType());

            using (var ms = new MemoryStream())
            {
                serializer.WriteObject(ms, objectState);

                var stringState = Encoding.Default.GetString(ms.ToArray());

                return stringState;
            }
        }

        private static object DeSerialize(ISerializerProvider provider, Type type, string stringState)
        {
            if (string.IsNullOrEmpty(stringState))
            {
                return null;
            }
            var serializer = provider.Provide(type);

            using (var ms = new MemoryStream(Encoding.Unicode.GetBytes(stringState)))
            {
                var objectState = serializer.ReadObject(ms);

                return objectState;
            }
        }
    }

    internal class JsonSerializerProvider : ISerializerProvider
    {
        public XmlObjectSerializer Provide(Type type)
        {
            return new DataContractJsonSerializer(type);
        }
    }
}
