﻿namespace Notifications.Topics
{
    using System;
    using System.Collections.Generic;

    using Notifications.Core;
    using Notifications.Core.Dto;

    public class BlogTopic : TopicInfo
    {
        public Guid Author { get; set; }

        public Guid BlogId { get; set; }

        public override List<Tuple<ActionType, DeviceType, string>> Templates
        {
            get
            {
                return new List<Tuple<ActionType, DeviceType, string>>()
                    {
                        new Tuple<ActionType, DeviceType, string>(
                            ActionType.Deleted,
                            DeviceType.Email,
                            "Dear ${User}, The User $Author has deleted the blog ${BlogUrl} on $DateDeleted."), 
                        new Tuple<ActionType, DeviceType, string>(
                            ActionType.Updated,
                            DeviceType.Email,
                            "Dear ${User}, The User ${Author} has updated the blog ${BlogUrl} on ${DateUpdated}."),
                    };
            }
        }

        public BlogTopic(Guid author, Guid blogId)
        {
            Author = author;
            BlogId = blogId;
        }

        public override string GenerateTopicName()
        {
            return string.Format("Blogs.{0}.{1}", this.Author, BlogId);
        }

    }
}