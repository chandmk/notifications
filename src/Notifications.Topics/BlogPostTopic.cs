namespace Notifications.Topics
{
    using System;
    using System.Collections.Generic;

    using Notifications.Core;

    public class BlogPostTopic : BlogTopic
    {
        private readonly Guid blogPostId;

        public BlogPostTopic(Guid author, Guid blogId, Guid blogPostId) : base(author, blogId)
        {
            this.blogPostId = blogPostId;
        }

        public override List<Tuple<ActionType, DeviceType, string>> Templates
        {
            get
            {
                return new List<Tuple<ActionType, DeviceType, string>>()
                    {
                        new Tuple<ActionType, DeviceType, string>(
                            ActionType.Created,
                            DeviceType.Email,
                            "Dear ${User}, The user ${AuthorName} has posted the blog ${BlogPostUrl} on ${DateAdded.ToShortDateString()}.  This post is currently ${Privacy}."), 
                        new Tuple<ActionType, DeviceType, string>(
                            ActionType.Updated,
                            DeviceType.Email,
                            "Dear ${User}, The user ${AuthorName} has updated the blog post ${BlogPostUrl} on ${DateUpdated.ToShortDateString()}.  This post is currently ${Privacy}."),
                            new Tuple<ActionType, DeviceType, string>(
                            ActionType.Created,
                            DeviceType.Email,
                            "Dear ${User}, The user ${AuthorName} has deleted the blog post ${BlogPostUrl} on ${DateDeleted.ToShortDateString()}.  This post is currently ${Privacy}."), 
                    };
            }
        }

        public override string GenerateTopicName()
        {
            return string.Format("{0}.{1}", base.GenerateTopicName(), blogPostId);
        }
    }

    [Serializable]
    public class BlogPostTemplateData
    {
        public BlogPostTemplateData(string authorName, string blogUrl, string privacy = "Public")
        {
            AuthorName = authorName;
            this.BlogPostUrl = blogUrl;
            Privacy = privacy;
        }

        public string AuthorName { get; set; }
        public string BlogPostUrl { get; set; }

        private DateTime dateAdded = DateTime.Now;

        public DateTime DateAdded
        {
            get
            {
                return this.dateAdded;
            }
            set
            {
                this.dateAdded = value;
            }
        }

        private DateTime dateUpdated = DateTime.Now;

        public DateTime DateUpdated
        {
            get
            {
                return this.dateUpdated;
            }
            set
            {
                this.dateUpdated = value;
            }
        }

        private DateTime dateDeleted = DateTime.Now;

        public DateTime DateDeleted
        {
            get
            {
                return this.dateDeleted;
            }
            set
            {
                this.dateDeleted = value;
            }
        }

        public string Privacy { get; set; }
    }
}