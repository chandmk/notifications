namespace Notifications.Topics
{
    using System;

    public class BlogPostCommentTopic : BlogPostTopic
    {
        public BlogPostCommentTopic(Guid author, Guid blogId, Guid blogPostId) : base(author, blogId, blogPostId)
        {
        }

        public override string GenerateTopicName()
        {
            return string.Format("{0}.{1}", base.GenerateTopicName(), "Comment" + Action);
        }
    }
}